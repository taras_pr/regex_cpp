package ua.com.logreader;

public interface SearchCallback {
    void onSuccess(String src);

    void onFinish(int count);
}
