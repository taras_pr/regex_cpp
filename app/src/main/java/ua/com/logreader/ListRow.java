package ua.com.logreader;

public class ListRow {
    public String text;
    public boolean checked;

    public ListRow(String text, boolean checked) {
        this.text = text;
        this.checked = checked;
    }
}
