#include <cstdlib>

#ifndef CPATTERNNODE_H
#define CPATTERNNODE_H

class PatternNode {
public:
    char type;
    size_t size;
    size_t offset;

    PatternNode(size_t offset, size_t size, char type);
    ~PatternNode();

    PatternNode* prev;
    PatternNode* next;

};

#endif


