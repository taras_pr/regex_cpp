#include "patternnode.h"

#ifndef PATTERN_H
#define PATTERN_H

class Pattern {
public:
    Pattern(char* str, size_t size);
    void Add(PatternNode* node);
    ~Pattern();
    
    size_t size;
    PatternNode* first;
    PatternNode* last;
    char* src;
    size_t src_size;
    size_t payload;
};

#endif

