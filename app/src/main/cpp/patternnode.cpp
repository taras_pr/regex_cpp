#include "patternnode.h"

PatternNode::PatternNode(size_t offset, size_t size, char type) {
    prev = next = 0;
    this->offset = offset;
    this->size = size;
    this->type = type;
};

PatternNode::~PatternNode() {

};
